# Insecure Direct Object References - idor

idor provides with an easy way to encrypt String values of DTOs using annotations.

## Build status
[![Build Status](https://drone.io/bitbucket.org/richard_hauswald/idor/status.png)](https://drone.io/bitbucket.org/richard_hauswald/idor/latest)

## Usage
###Include as maven dependency
idor is available on central and can be included as maven dependency using the following snippet:
```xml
<dependency>
    <groupId>de.rhauswald.security.idor</groupId>
    <artifactId>idor</artifactId>
    <version>${your desired version}</version>
</dependency>
```
Released versions are tagged using the version number. Choose the most recent one from the tag list in the box on the
right side of this page. As soon as the jgitflow-maven-plugin supports updating version numbers in README.md files this
paragraph will be updated with the latest version with each release.
