/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import com.gs.collections.api.block.predicate.Predicate;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;

class SupportedPrimitiveTypePredicate implements Predicate<FieldEditorInformation> {
    private SupportedPrimitiveTypePredicate() {
    }

    @Override
	public boolean accept(FieldEditorInformation fieldEditorInformation) {
		final Class<?> inputType = fieldEditorInformation.getField().getType();
		if (String.class.equals(inputType)) {
			return true;
		}
		if (inputType.isArray()) {
			return true;
		}
		if (inputType.isPrimitive()) {
			return false;
		}
		final Package inputTypePackage = inputType.getPackage();
		final String inputTypePackageName = inputTypePackage.getName();
		final boolean startsWith = inputTypePackageName.startsWith("java.lang");
		return !startsWith;
	}

    public static SupportedPrimitiveTypePredicate acceptOnlySupportedPrimitiveTypes() {
        return new SupportedPrimitiveTypePredicate();
    }
}
