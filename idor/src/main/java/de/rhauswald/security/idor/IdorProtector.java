/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor;

import com.gs.collections.api.set.ImmutableSet;
import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditor;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;
import de.rhauswald.security.idor.internal.scanner.ProtectedFieldScanner;

import java.util.HashMap;
import java.util.Map;

public class IdorProtector {
    private final EncryptionComponent encryptionComponent;

    public IdorProtector(EncryptionComponent encryptionComponent) {
        this.encryptionComponent = encryptionComponent;
    }

    public void protectDto(Object dto) {
        final Map<String, String> encryptedValues = new HashMap<>();
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(dto);
        for (DtoFieldInformation fieldInformation : dtoFieldInformation) {
            final FieldEditorInformation fieldEditorInformation = fieldInformation.getFieldEditorInformation();
            final Object declaringDto = fieldInformation.getDto();
            final String value = (String) FieldEditor.getValue(fieldEditorInformation, declaringDto);
            final String encrypted;
            if (encryptedValues.containsKey(value)) {
                encrypted = encryptedValues.get(value);
            } else {
                encrypted = encryptionComponent.encryptToBase64(value);
                encryptedValues.put(value, encrypted);
            }
            FieldEditor.setValue(fieldEditorInformation, declaringDto, encrypted);
        }
        /*
        TODO
		- Clean up FieldEditorInformation
		- Make a clean mapping between these two to ensure separation
		 */
        /*
        protectedFieldScanner.scanForIdorProtectedFields(dto)
				.forEach(new Procedure<DtoFieldInformation>() {
					@Override
					public void value(DtoFieldInformation dtoFieldInformation) {
						final String value = (String) dtoFieldInformation.getValue();
						final CipherText cipherText = encryptor.encrypt(secretKey, new PlainText(value));
						fieldEditor.setValue(dtoFieldInformation.getFieldEditorInformation(), );
					}
				});
		 */
    }

    public void decryptProtectedDto(Object dto) {
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(dto);
        for (DtoFieldInformation fieldInformation : dtoFieldInformation) {
            final FieldEditorInformation fieldEditorInformation = fieldInformation.getFieldEditorInformation();
            final Object declaringDto = fieldInformation.getDto();
            final String value = (String) FieldEditor.getValue(fieldEditorInformation, declaringDto);
            final String encrypted;
            try {
                encrypted = encryptionComponent.decryptFromBase64(value);
            } catch (RuntimeException e) {
                throw new UnsupportedOperationException("Could not decrypt field " + fieldInformation, e);
            }
            FieldEditor.setValue(fieldEditorInformation, declaringDto, encrypted);
        }
    }
}
