/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import com.gs.collections.api.block.function.Function;
import de.rhauswald.security.idor.internal.DtoFieldInformation;

class DtoFieldInformationToValueMapper implements Function<DtoFieldInformation, Object> {
    private DtoFieldInformationToValueMapper() {
    }

    @Override
	public Object valueOf(DtoFieldInformation object) {
		return object.getValue();
	}

    public static DtoFieldInformationToValueMapper extractValueProperty() {
        return new DtoFieldInformationToValueMapper();
    }
}
