/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal;

import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;

import java.util.Objects;

public class DtoFieldInformation {
    private final FieldEditorInformation fieldEditorInformation;
    private final Object value;
    private final Object dto;

    public DtoFieldInformation(Object dto, FieldEditorInformation fieldEditorInformation, Object value) {
        this.dto = dto;
        this.fieldEditorInformation = fieldEditorInformation;
        this.value = value;
    }

    public FieldEditorInformation getFieldEditorInformation() {
        return fieldEditorInformation;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DtoFieldInformation that = (DtoFieldInformation) o;
        return Objects.equals(fieldEditorInformation, that.fieldEditorInformation) &&
                Objects.equals(value, that.value) &&
                Objects.equals(dto, that.dto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldEditorInformation, value, dto);
    }

    @Override
    public String toString() {
        return "DtoFieldInformation{" +
                "fieldEditorInformation=" + fieldEditorInformation +
                ", value=" + value +
                '}';
    }

    public Object getDto() {
        return dto;
    }
}
