/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import com.gs.collections.api.block.function.Function;
import com.gs.collections.api.partition.set.PartitionMutableSet;
import com.gs.collections.api.set.ImmutableSet;
import com.gs.collections.api.set.MutableSet;
import com.gs.collections.impl.factory.Sets;
import com.gs.collections.impl.set.mutable.UnifiedSet;
import de.rhauswald.security.idor.IdorProtectedField;
import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.internal.annotations.DtoFieldInformationWithDeclaredAnnotationPredicate;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;
import org.reflections.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static de.rhauswald.security.idor.internal.fieldeditor.FieldToFieldFieldEditorInformation.mapFieldsToFieldEditorInformation;
import static de.rhauswald.security.idor.internal.fieldeditor.SupportedFieldEditorInformationPredicate.acceptOnlySupportedFields;
import static de.rhauswald.security.idor.internal.scanner.DtoFieldInformationFactory.mapToDtoFieldInformation;
import static de.rhauswald.security.idor.internal.scanner.DtoFieldInformationNullValuePredicate.acceptOnlyNonNullFieldInformation;
import static de.rhauswald.security.idor.internal.scanner.DtoFieldInformationToValueMapper.extractValueProperty;
import static de.rhauswald.security.idor.internal.scanner.SupportedPrimitiveTypePredicate.acceptOnlySupportedPrimitiveTypes;

public final class ProtectedFieldScanner {
    private ProtectedFieldScanner() {
    }

    public static ImmutableSet<DtoFieldInformation> scanForIdorProtectedFields(final Object dto) {
        return scanForIdorProtectedFields(dto, Sets.mutable.with());
    }

    private static ImmutableSet<DtoFieldInformation> scanForIdorProtectedFields(final Object dto, final MutableSet<Object> visited) {
        if (dto == null || !visited.add(dto)) {
            return Sets.immutable.with();
        }
        if (dto instanceof Iterable) {
            final Iterable<?> iterable = (Iterable) dto;
            return Sets.immutable
                    .ofAll(iterable)
                    .flatCollect(new RecursiveCall(visited));
        }
        if (dto instanceof Map) {
            final Map map = (Map) dto;
            final Set<?> keySet = map.keySet();
            final Collection<?> values = map.values();
            return Sets.mutable
                    .withAll(keySet)
                    .withAll(values)
                    .flatCollect(new RecursiveCall(visited))
                    .toImmutable();
        }
        if (dto instanceof Object[]) {
            final Object[] array = (Object[]) dto;
            return Sets.immutable
                    .with(array)
                    .flatCollect(new RecursiveCall(visited));
        }

        final UnifiedSet<Field> allFieldsOfDto = allFieldsOfDto(dto.getClass());
        final UnifiedSet<FieldEditorInformation> allFieldsOfDtoFieldEditorInformations = allFieldsOfDto.collect(mapFieldsToFieldEditorInformation());
        final UnifiedSet<FieldEditorInformation> supportedTypesFieldsOfDtoFieldEditorInformation = allFieldsOfDtoFieldEditorInformations.select(acceptOnlySupportedFields());
        final UnifiedSet<FieldEditorInformation> select = supportedTypesFieldsOfDtoFieldEditorInformation.select(acceptOnlySupportedPrimitiveTypes());
        final PartitionMutableSet<DtoFieldInformation> partitionByIdorProtectedAnnotation = select
                .collectWith(mapToDtoFieldInformation(), dto)
                .reject(acceptOnlyNonNullFieldInformation())
                .partition(DtoFieldInformationWithDeclaredAnnotationPredicate.acceptOnlyFieldsWithAnnotation(IdorProtectedField.class));

        return Sets.mutable
                .withAll(partitionByIdorProtectedAnnotation.getSelected())
                .withAll(partitionByIdorProtectedAnnotation.getRejected()
                        .collect(extractValueProperty())
                        .flatCollect(new RecursiveCall(visited)))
                .toImmutable();
    }

    @SuppressWarnings("unchecked")
    private static UnifiedSet<Field> allFieldsOfDto(Class<?> dtoClass) {
        final Set<Field> allFields = ReflectionUtils.getAllFields(dtoClass);
        return UnifiedSet.newSet(allFields);
    }

    private static final class RecursiveCall implements Function<Object, ImmutableSet<DtoFieldInformation>> {
        private static final long serialVersionUID = -7919865437777644026L;
        private final MutableSet<Object> visited;

        private RecursiveCall(MutableSet<Object> visited) {
            this.visited = visited;
        }

        @Override
        public ImmutableSet<DtoFieldInformation> valueOf(Object object) {
            final ImmutableSet<DtoFieldInformation> dtoFieldInformation = scanForIdorProtectedFields(object, visited);
            return dtoFieldInformation;
        }
    }
}
