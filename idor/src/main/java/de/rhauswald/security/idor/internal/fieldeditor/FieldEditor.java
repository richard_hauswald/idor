/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.fieldeditor;

import com.gs.collections.api.block.function.Function0;
import com.gs.collections.api.map.MutableMap;
import com.gs.collections.impl.factory.Maps;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class FieldEditor {
    private static final MutableMap<Field, FieldEditorInformation> cache = Maps.mutable.of();

    public static void setValue(FieldEditorInformation fieldEditorInformation, Object instance, Object value) {
        final Method setter = fieldEditorInformation.getSetter();
        try {
            setter.invoke(instance, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new UnexpectedErrorException("Unexpected error setting field value", e);
        }
    }

    public static Object getValue(FieldEditorInformation fieldEditorInformation, Object instance) {
        final Method getter = fieldEditorInformation.getGetter();
        try {
            final Object value = getter.invoke(instance);
            return value;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new UnexpectedErrorException("Unexpected error setting field value", e);
        }
    }

    public static FieldEditorInformation analyze(final Field field) {
        final FieldEditorInformation fieldEditorInformation = cache.getIfAbsentPut(field, new FieldEditorInformationAnalyzer(field));
        return fieldEditorInformation;
    }

    private static FieldEditorInformation doAnalyze(Field field) {
        try {
            final PropertyDescriptor propertyDescriptor = PropertyDescriptorFactory.fromField(field);
            final Method readMethod = propertyDescriptor.getReadMethod();
            final Method writeMethod = propertyDescriptor.getWriteMethod();
            return new SupportedFieldEditorInformation(field, readMethod, writeMethod);
        } catch (PropertyDescriptorFactory.UnsupportedFieldException ignored) {
            return new UnsupportedFieldEditorInformation(field);
        }
    }

    public static class UnexpectedErrorException extends RuntimeException {
        private static final long serialVersionUID = -5821431815182980367L;

        public UnexpectedErrorException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private static final class FieldEditorInformationAnalyzer implements Function0<FieldEditorInformation> {
        private static final long serialVersionUID = 8257077899777763025L;
        private final Field field;

        private FieldEditorInformationAnalyzer(Field field) {
            this.field = field;
        }

        @Override
        public FieldEditorInformation value() {
            return doAnalyze(field);
        }
    }
}
