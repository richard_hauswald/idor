package de.rhauswald.security.idor.stubs.complex;

import de.rhauswald.security.idor.IdorProtectedField;

import java.io.Serializable;
import java.util.UUID;

public class Identifier implements Serializable {
    private static final long serialVersionUID = 313541892635192649L;

    @IdorProtectedField
    private String id;

    public Identifier(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Identifier{" +
                "id='" + id + '\'' +
                '}';
    }
}
