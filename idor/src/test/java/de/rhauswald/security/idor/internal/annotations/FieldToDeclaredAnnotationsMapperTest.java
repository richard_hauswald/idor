/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.annotations;

import com.gs.collections.api.list.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.lang.annotation.*;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class FieldToDeclaredAnnotationsMapperTest {

	private FieldToDeclaredAnnotationsMapper sut;

	@Before
	public void setUp() throws Exception {
		sut = new FieldToDeclaredAnnotationsMapper();
	}

	@Test
	public void testNoAnnotation() throws Exception {
		final ImmutableList<Annotation> annotations = sut.declaredAnnotations(Stub.class.getDeclaredField("id1"));
		assertThat(annotations, is(Matchers.<Annotation>iterableWithSize(0)));
	}

	@Test
	public void testSingleAnnotation() throws Exception {
		final ImmutableList<Annotation> annotations = sut.declaredAnnotations(Stub.class.getDeclaredField("id2"));
		assertThat(annotations, is(Matchers.<Annotation>iterableWithSize(1)));
	}

	@Test
	public void testMultipleAnnotations() throws Exception {
		final ImmutableList<Annotation> annotations = sut.declaredAnnotations(Stub.class.getDeclaredField("id3"));
		assertThat(annotations, is(Matchers.<Annotation>iterableWithSize(2)));
	}

	@SuppressWarnings("UnusedDeclaration")
	private static class Stub {
		private String id1;
		@StubAnnotation1
		private String id2;
		@StubAnnotation1
		@StubAnnotation2
		private String id3;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	private static @interface StubAnnotation1 {
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	private static @interface StubAnnotation2 {
	}
}
