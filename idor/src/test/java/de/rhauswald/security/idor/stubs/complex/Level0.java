package de.rhauswald.security.idor.stubs.complex;

import java.io.Serializable;
import java.util.List;

public class Level0 implements Serializable {
    private static final long serialVersionUID = -934723116200972755L;

    private Identifier ownIdentifier;
    private String description;
    private Identifier foreignKeyOne;
    private List<Child1> hostGroups;

    public Identifier getOwnIdentifier() {
        return ownIdentifier;
    }

    public void setOwnIdentifier(Identifier ownIdentifier) {
        this.ownIdentifier = ownIdentifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Identifier getForeignKeyOne() {
        return foreignKeyOne;
    }

    public void setForeignKeyOne(Identifier foreignKeyOne) {
        this.foreignKeyOne = foreignKeyOne;
    }

    public List<Child1> getHostGroups() {
        return hostGroups;
    }

    public void setHostGroups(List<Child1> fqdns) {
        this.hostGroups = fqdns;
    }

    @Override
    public String toString() {
        return "Level0{" +
                "ownIdentifier=" + ownIdentifier +
                ", description='" + description + '\'' +
                ", foreignKeyOne=" + foreignKeyOne +
                ", hostGroups=" + hostGroups +
                '}';
    }
}

