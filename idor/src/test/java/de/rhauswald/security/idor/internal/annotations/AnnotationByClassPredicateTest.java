/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.annotations;

import de.rhauswald.security.idor.IdorProtectedField;
import org.junit.Test;

import java.lang.annotation.*;
import java.lang.reflect.Field;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AnnotationByClassPredicateTest {
	@Test
	public void testAccept() throws Exception {
		final Class<?> stubClass = Stub.class;
		final Field field = stubClass.getDeclaredField("id1");
		final Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
		final Annotation IdorProtectedFieldAnnotation = declaredAnnotations[0];
		final boolean accept = AnnotationByClassPredicate.acceptOnlyAnnotation(IdorProtectedField.class).accept(IdorProtectedFieldAnnotation);
		assertThat(accept, is(true));
	}

	@Test
	public void testReject() throws Exception {
		final Class<?> stubClass = Stub.class;
		final Field field = stubClass.getDeclaredField("id2");
		final Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
		final Annotation IdorProtectedFieldAnnotation = declaredAnnotations[0];
		final boolean accept = AnnotationByClassPredicate.acceptOnlyAnnotation(IdorProtectedField.class).accept(IdorProtectedFieldAnnotation);
		assertThat(accept, is(false));
	}

	@SuppressWarnings("UnusedDeclaration")
	private static class Stub {
		@IdorProtectedField
		private String id1;
		@DisturbanceStub
		private String id2;
	}
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	private static @interface DisturbanceStub {}
}
