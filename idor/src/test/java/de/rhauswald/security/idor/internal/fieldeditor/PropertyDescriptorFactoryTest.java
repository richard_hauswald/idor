/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.fieldeditor;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class PropertyDescriptorFactoryTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void validProperty() throws Exception {
		final PropertyDescriptor propertyDescriptor = getPropertyDescriptor("validProperty");
		assertThat(propertyDescriptor, is(notNullValue()));
		assertThat(propertyDescriptor.getReadMethod(), is(notNullValue()));
		assertThat(propertyDescriptor.getWriteMethod(), is(notNullValue()));
	}

	@Test
	public void noGetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("noGetter");
	}

	@Test
	public void privateGetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("privateGetter");
	}

	@Test
	public void protectedGetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("protectedGetter");
	}

	@Test
	public void defaultGetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("defaultGetter");
	}

	@Test
	public void noSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("noSetter");
	}

	@Test
	public void privateSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("privateSetter");
	}

	@Test
	public void protectedSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("protectedSetter");
	}

	@Test
	public void defaultSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("defaultSetter");
	}

	@Test
	public void staticGetterAndSetterReturnsADescriptorWithoutComplaining() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("staticGetterAndSetter");
	}

	@Test
	public void privateGetterAndSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("privateGetterAndSetter");
	}

	@Test
	public void protectedGetterAndSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("protectedGetterAndSetter");
	}

	@Test
	public void defaultGetterAndSetter() throws Exception {
		expectedException.expect(PropertyDescriptorFactory.UnsupportedFieldException.class);
		getPropertyDescriptor("defaultGetterAndSetter");
	}

	private PropertyDescriptor getPropertyDescriptor(String propertyName) throws PropertyDescriptorFactory.UnsupportedFieldException, NoSuchFieldException {
		final Field declaredField = StubDto.class.getDeclaredField(propertyName);
		assertThat(declaredField, is(notNullValue()));
		return PropertyDescriptorFactory.fromField(declaredField);
	}
}
