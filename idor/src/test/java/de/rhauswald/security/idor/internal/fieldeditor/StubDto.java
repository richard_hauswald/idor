/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.fieldeditor;

@SuppressWarnings("ALL")
public class StubDto {
	private String validProperty;
	private String noGetter;
	private String privateGetter;
	private String protectedGetter;
	private String defaultGetter;
	private String noSetter;
	private String privateSetter;
	private String protectedSetter;
	private String defaultSetter;
	private String staticGetterAndSetter;
	private String privateGetterAndSetter;
	private String protectedGetterAndSetter;
	private String defaultGetterAndSetter;

	public String getValidProperty() {
		return validProperty;
	}

	public void setValidProperty(String validProperty) {
		this.validProperty = validProperty;
	}

	public void setNoGetter(String noGetter) {
		this.noGetter = noGetter;
	}

	private String getPrivateGetter() {
		return privateGetter;
	}

	public void setPrivateGetter(String privateGetter) {
		this.privateGetter = privateGetter;
	}

	protected String getProtectedGetter() {
		return protectedGetter;
	}

	public void setProtectedGetter(String protectedGetter) {
		this.protectedGetter = protectedGetter;
	}

	String getDefaultGetter() {
		return defaultGetter;
	}

	public void setDefaultGetter(String defaultGetter) {
		this.defaultGetter = defaultGetter;
	}

	public String getNoSetter() {
		return noSetter;
	}

	public String getPrivateSetter() {
		return privateSetter;
	}

	private void setPrivateSetter(String privateSetter) {
		this.privateSetter = privateSetter;
	}

	public String getProtectedSetter() {
		return protectedSetter;
	}

	protected void setProtectedSetter(String protectedSetter) {
		this.protectedSetter = protectedSetter;
	}

	public String getDefaultSetter() {
		return defaultSetter;
	}

	void setDefaultSetter(String defaultSetter) {
		this.defaultSetter = defaultSetter;
	}

	public static String getStaticGetterAndSetter() {
		return null;
	}

	public static void setStaticGetterAndSetter(String staticGetterAndSetter) {
	}

	private String getPrivateGetterAndSetter() {
		return null;
	}

	private void setPrivateGetterAndSetter(String privateGetterAndSetter) {
		this.privateGetterAndSetter = privateGetterAndSetter;
	}

	protected String getProtectedGetterAndSetter() {
		return protectedGetterAndSetter;
	}

	protected void setProtectedGetterAndSetter(String protectedGetterAndSetter) {
		this.protectedGetterAndSetter = protectedGetterAndSetter;
	}

	String getDefaultGetterAndSetter() {
		return defaultGetterAndSetter;
	}

	void setDefaultGetterAndSetter(String defaultGetterAndSetter) {
		this.defaultGetterAndSetter = defaultGetterAndSetter;
	}
}
