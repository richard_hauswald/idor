/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.stubs.nested;

import de.rhauswald.security.idor.IdorProtectedField;

public class NestedLevel1Dto {
	@IdorProtectedField
	private String id;
	private String nonProtectedField;
	private NestedLevel2Dto nestedLevel2Dto;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNonProtectedField() {
		return nonProtectedField;
	}

	public void setNonProtectedField(String nonProtectedField) {
		this.nonProtectedField = nonProtectedField;
	}

	public NestedLevel2Dto getNestedLevel2Dto() {
		return nestedLevel2Dto;
	}

	public void setNestedLevel2Dto(NestedLevel2Dto nestedLevel2Dto) {
		this.nestedLevel2Dto = nestedLevel2Dto;
	}
}
