/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor;

import de.rhauswald.security.idor.stubs.complex.Child1;
import de.rhauswald.security.idor.stubs.complex.Identifier;
import de.rhauswald.security.idor.stubs.complex.Level0;
import de.rhauswald.security.idor.stubs.nested.NestedLevel0Dto;
import de.rhauswald.security.idor.stubs.nested.NestedLevel1Dto;
import de.rhauswald.security.idor.stubs.nested.NestedLevel2Dto;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class IdorProtectorTest {

    private IdorProtector sut;

    @Before
    public void setUp() throws Exception {
        sut = new IdorProtector(EncryptionComponent.Factory.basicEncryptionComponent(SecretKeyProvider.Factory.basicSecretKeyProvider()));
    }

    @Test
    public void testProtectDto() throws Exception {
        final NestedLevel2Dto nestedLevel2Dto = new NestedLevel2Dto();
        nestedLevel2Dto.setId("99");
        nestedLevel2Dto.setNonProtectedField("nested level 2");

        final NestedLevel1Dto nestedLevel1Dto = new NestedLevel1Dto();
        nestedLevel1Dto.setId("42");
        nestedLevel1Dto.setNonProtectedField("nested level 1");
        nestedLevel1Dto.setNestedLevel2Dto(nestedLevel2Dto);

        final NestedLevel0Dto nestedLevel0Dto = new NestedLevel0Dto();
        nestedLevel0Dto.setId("23");
        nestedLevel0Dto.setNonProtectedField("nested level 0");
        nestedLevel0Dto.setNestedLevel1Dto(nestedLevel1Dto);

        sut.protectDto(nestedLevel0Dto);

        assertThat(nestedLevel0Dto.getId(), is(not("23")));
        assertThat(nestedLevel0Dto.getNonProtectedField(), is("nested level 0"));

        assertThat(nestedLevel1Dto.getId(), is(not("42")));
        assertThat(nestedLevel1Dto.getNonProtectedField(), is("nested level 1"));

        assertThat(nestedLevel2Dto.getId(), is(not("99")));
        assertThat(nestedLevel2Dto.getNonProtectedField(), is("nested level 2"));

        sut.decryptProtectedDto(nestedLevel0Dto);
        assertThat(nestedLevel0Dto.getId(), is("23"));
        assertThat(nestedLevel0Dto.getNonProtectedField(), is("nested level 0"));

        assertThat(nestedLevel1Dto.getId(), is("42"));
        assertThat(nestedLevel1Dto.getNonProtectedField(), is("nested level 1"));

        assertThat(nestedLevel2Dto.getId(), is("99"));
        assertThat(nestedLevel2Dto.getNonProtectedField(), is("nested level 2"));
    }

    @Test
    public void testComplexDto() throws Exception {
        final Level0 level0 = new Level0();
        level0.setOwnIdentifier(new Identifier("level0OwnUuid"));
        level0.setDescription("Level0 description");
        final String foreignKeyOne = "child1Identifier";
        level0.setForeignKeyOne(new Identifier(foreignKeyOne));
        level0.setHostGroups(Arrays.asList(new Child1("child1Identifier")));

        sut.protectDto(level0);
        assertThat(level0.getOwnIdentifier(), is(not(new Identifier("level0OwnUuid"))));
        assertThat(level0.getForeignKeyOne(), is(not(new Identifier("child1Identifier"))));

        sut.decryptProtectedDto(level0);
        assertThat(level0.getOwnIdentifier().getId(), is(new Identifier("level0OwnUuid").getId()));
        assertThat(level0.getForeignKeyOne().getId(), is(new Identifier(foreignKeyOne).getId()));
        assertThat(level0.getHostGroups().get(0).getIdentifier().getId(), is(new Identifier("child1Identifier").getId()));
    }
}
