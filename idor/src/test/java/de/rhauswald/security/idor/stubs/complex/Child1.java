package de.rhauswald.security.idor.stubs.complex;

import java.io.Serializable;
import java.util.UUID;

public class Child1 implements Serializable {
    private static final long serialVersionUID = -3766874544614756128L;

    private Identifier identifier;
    private String payload;

    public Child1(String id) {
        identifier = new Identifier(id);
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "Child1{" +
                "identifier=" + identifier +
                ", payload='" + payload + '\'' +
                '}';
    }
}
