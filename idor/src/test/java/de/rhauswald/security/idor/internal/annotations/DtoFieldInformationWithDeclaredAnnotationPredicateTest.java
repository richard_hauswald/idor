/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.annotations;

import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;
import org.junit.Before;
import org.junit.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DtoFieldInformationWithDeclaredAnnotationPredicateTest {
    private DtoFieldInformationWithDeclaredAnnotationPredicate sut;

    @Before
    public void setUp() throws Exception {
        sut = DtoFieldInformationWithDeclaredAnnotationPredicate.acceptOnlyFieldsWithAnnotation(StubAnnotation1.class);
    }

    @Test
    public void testAcceptsWithOnlyRequiredAnnotationPresent() throws Exception {
        final boolean accept = sut.accept(supportedFieldInformationForField("id2"));
        assertThat(accept, is(true));
    }

    @Test
    public void testAcceptsWithMultipleAnnotationsAndRequiredAnnotationPresent() throws Exception {
        final boolean accept = sut.accept(supportedFieldInformationForField("id3"));
        assertThat(accept, is(true));
    }

    @Test
    public void testRejectsWithNoAnnotationPresent() throws Exception {
        final boolean accept = sut.accept(supportedFieldInformationForField("id1"));
        assertThat(accept, is(false));
    }

    @Test
    public void testRejectsWithMultipleAnnotationsPresentButRequiredAnnotationMissing() throws Exception {
        final boolean accept = sut.accept(supportedFieldInformationForField("id4"));
        assertThat(accept, is(false));
    }

    private DtoFieldInformation supportedFieldInformationForField(String fieldName) throws NoSuchFieldException {
        return new DtoFieldInformation(null, new FieldEditorInformationStub(Stub.class.getDeclaredField(fieldName)), null);
    }

    @SuppressWarnings("UnusedDeclaration")
    private static class Stub {
        private String id1;
        @StubAnnotation1
        private String id2;
        @StubAnnotation1
        @StubAnnotation2
        private String id3;
        @StubAnnotation2
        @StubAnnotation3
        private String id4;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    private @interface StubAnnotation1 {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    private @interface StubAnnotation2 {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    private @interface StubAnnotation3 {
    }

    private static final class FieldEditorInformationStub implements FieldEditorInformation {
        private final Field field;

        private FieldEditorInformationStub(Field field) {
            this.field = field;
        }

        @Override
        public Field getField() {
            return field;
        }

        @Override
        public Method getGetter() {
            return null;
        }

        @Override
        public Method getSetter() {
            return null;
        }

        @Override
        public boolean isSupported() {
            return false;
        }
    }
}
