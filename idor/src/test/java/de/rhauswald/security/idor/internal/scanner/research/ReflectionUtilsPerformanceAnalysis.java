/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner.research;

import com.google.common.base.Stopwatch;
import de.rhauswald.security.idor.stubs.nested.NestedLevel0Dto;
import org.reflections.ReflectionUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unchecked")
public class ReflectionUtilsPerformanceAnalysis {
	public static void main(String[] args) throws NoSuchFieldException {
		final Class<NestedLevel0Dto> type = NestedLevel0Dto.class;
		final Cache cache = new Cache();
		final int analysisToMake = 1000000;
		Stopwatch stopwatch = Stopwatch.createStarted();
		for (int i = 0; i < analysisToMake; i++) {
			ReflectionUtils.getAllFields(type);
		}
		stopwatch.stop();
		final long elapsedPlain = stopwatch.elapsed(TimeUnit.NANOSECONDS);
		stopwatch.reset();
		stopwatch.start();
		for (int i = 0; i < analysisToMake; i++) {
			cache.getAllFields(type);
		}
		final long elapsedCached = stopwatch.stop().elapsed(TimeUnit.NANOSECONDS);
		final long speedAdvantageByCaching = elapsedPlain - elapsedCached;
		final long speedAdvantageByCachingInMs = TimeUnit.MILLISECONDS.convert(speedAdvantageByCaching, TimeUnit.NANOSECONDS);
		final BigDecimal bigDecimal = new BigDecimal(elapsedPlain);
		final BigDecimal divisor = new BigDecimal(elapsedCached);
		final BigDecimal divide = bigDecimal.divide(divisor, 4, BigDecimal.ROUND_HALF_UP);
		final String message = "Performance boots of caching: " +
				+ divide.multiply(new BigDecimal(100)).doubleValue() + "% - " + speedAdvantageByCachingInMs + "ms faster " +
				"for " + analysisToMake + " analysis calls. This proves that ReflectionUtils is already caching";
		System.out.println(message);
	}

	private static class Cache {
		private java.util.Set<Field> getAllFields(Class<?> type) {
			return ReflectionUtils.getAllFields(type);
		}
	}
}
