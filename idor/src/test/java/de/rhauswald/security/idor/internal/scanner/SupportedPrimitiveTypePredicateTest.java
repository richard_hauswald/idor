/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SupportedPrimitiveTypePredicateTest {
	private SupportedPrimitiveTypePredicate sut;

	@Before
	public void setUp() throws Exception {
		sut = SupportedPrimitiveTypePredicate.acceptOnlySupportedPrimitiveTypes();
	}

	@Test
	public void acceptsStringField() throws Exception {
		assertThat(actWith("stringField"), is(true));
	}

	@Test
	public void acceptsStubField() throws Exception {
		assertThat(actWith("stubField"), is(true));
	}

	@Test
	public void acceptsCollectionField() throws Exception {
		assertThat(actWith("collectionField"), is(true));
	}

	@Test
	public void acceptsListField() throws Exception {
		assertThat(actWith("listField"), is(true));
	}

	@Test
	public void acceptsSetField() throws Exception {
		assertThat(actWith("setField"), is(true));
	}

	@Test
	public void rejectsIntField() throws Exception {
		assertThat(actWith("intField"), is(false));
	}

	@Test
	public void rejectsIntegerField() throws Exception {
		assertThat(actWith("integerField"), is(false));
	}

	@Test
	public void rejectsDoubleField() throws Exception {
		assertThat(actWith("doubleField"), is(false));
	}

	@Test
	public void rejectsDoubleObjectField() throws Exception {
		assertThat(actWith("doubleObjectField"), is(false));
	}

	private boolean actWith(String fieldName) throws NoSuchFieldException {
		return sut.accept(fieldEditorInformation(fieldName));
	}

	private FieldEditorInformationStub fieldEditorInformation(String fieldName) throws NoSuchFieldException {
		return new FieldEditorInformationStub(Stub.class.getDeclaredField(fieldName));
	}

	@SuppressWarnings("UnusedDeclaration")
	public static class Stub {
		private String stringField;
		private Stub stubField;
		private Collection<?> collectionField;
		private List<?> listField;
		private Set<?> setField;
		private int intField;
		private Integer integerField;
		private double doubleField;
		private Double doubleObjectField;
	}

    private static class FieldEditorInformationStub implements FieldEditorInformation {
        private final Field field;

        private FieldEditorInformationStub(Field field) {
            this.field = field;
        }

        @Override
        public Field getField() {
            return field;
        }

        @Override
        public Method getGetter() {
            return null;
        }

        @Override
        public Method getSetter() {
            return null;
        }

        @Override
        public boolean isSupported() {
            return false;
        }
    }
}
