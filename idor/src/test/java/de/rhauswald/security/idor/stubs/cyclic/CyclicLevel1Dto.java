/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.stubs.cyclic;

import de.rhauswald.security.idor.IdorProtectedField;

public class CyclicLevel1Dto {
	@IdorProtectedField
	private String id;
	private String nonProtectedField;
	private CyclicLevel2Dto cyclicLevel2Dto;
	private CyclicLevel0Dto root;
	private CyclicLevel0Dto parent;
	private CyclicLevel2Dto child;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNonProtectedField() {
		return nonProtectedField;
	}

	public void setNonProtectedField(String nonProtectedField) {
		this.nonProtectedField = nonProtectedField;
	}

	public CyclicLevel2Dto getCyclicLevel2Dto() {
		return cyclicLevel2Dto;
	}

	public void setCyclicLevel2Dto(CyclicLevel2Dto cyclicLevel2Dto) {
		this.cyclicLevel2Dto = cyclicLevel2Dto;
	}

	public void setRoot(CyclicLevel0Dto root) {
		this.root = root;
	}

	public CyclicLevel0Dto getRoot() {
		return root;
	}

	public void setParent(CyclicLevel0Dto parent) {
		this.parent = parent;
	}

	public CyclicLevel0Dto getParent() {
		return parent;
	}

	public void setChild(CyclicLevel2Dto child) {
		this.child = child;
	}

	public CyclicLevel2Dto getChild() {
		return child;
	}
}
