/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.internal.fieldeditor.FieldEditorInformation;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.lang.reflect.Field;

class DtoFieldInformationWithNameAndValueMatcher extends TypeSafeMatcher<DtoFieldInformation> {
	private final String expectedName;
	private final Object expectedValue;
	private String actualName;
	private Object actualValue;

	static DtoFieldInformationWithNameAndValueMatcher aDtoFieldInformationWithNameAndValue(String expectedName, Object expectedValue) {
		return new DtoFieldInformationWithNameAndValueMatcher(expectedName, expectedValue);
	}

	DtoFieldInformationWithNameAndValueMatcher(String expectedName, Object expectedValue) {
		this.expectedName = expectedName;
		this.expectedValue = expectedValue;
	}

	@Override
	protected boolean matchesSafely(DtoFieldInformation item) {
		final FieldEditorInformation fieldEditorInformation = item.getFieldEditorInformation();
		final Field field = fieldEditorInformation.getField();
		actualName = field.getName();
		actualValue = item.getValue();
		boolean nameMatches = expectedName.equals(actualName);
		boolean valueMatches = expectedValue.equals(actualValue);
		return nameMatches && valueMatches;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("Expected a dtoFieldInformation with " +
				"name " + expectedName + "(actual: " + actualName + ") and " +
				"value " + expectedValue + "(actual: " + actualValue + ")");
	}
}
