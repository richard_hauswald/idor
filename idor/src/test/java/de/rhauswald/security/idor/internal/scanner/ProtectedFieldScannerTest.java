/*
 * Copyright 2014-2014 Richard Hauswald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.rhauswald.security.idor.internal.scanner;

import com.google.common.base.Stopwatch;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.api.set.ImmutableSet;
import de.rhauswald.security.idor.internal.DtoFieldInformation;
import de.rhauswald.security.idor.stubs.collections.arrays.ArrayLevel0Dto;
import de.rhauswald.security.idor.stubs.collections.arrays.ArrayLevel1Dto;
import de.rhauswald.security.idor.stubs.collections.lists.ListLevel0Dto;
import de.rhauswald.security.idor.stubs.collections.lists.ListLevel1Dto;
import de.rhauswald.security.idor.stubs.collections.maps.MapLevel0Dto;
import de.rhauswald.security.idor.stubs.collections.maps.MapLevel1KeyDto;
import de.rhauswald.security.idor.stubs.collections.maps.MapLevel1ValueDto;
import de.rhauswald.security.idor.stubs.collections.sets.SetLevel0Dto;
import de.rhauswald.security.idor.stubs.collections.sets.SetLevel1Dto;
import de.rhauswald.security.idor.stubs.cyclic.CyclicLevel0Dto;
import de.rhauswald.security.idor.stubs.cyclic.CyclicLevel1Dto;
import de.rhauswald.security.idor.stubs.cyclic.CyclicLevel2Dto;
import de.rhauswald.security.idor.stubs.flat.FlatDto;
import de.rhauswald.security.idor.stubs.inheritance.DerivedDto;
import de.rhauswald.security.idor.stubs.nested.NestedLevel0Dto;
import de.rhauswald.security.idor.stubs.nested.NestedLevel1Dto;
import de.rhauswald.security.idor.stubs.nested.NestedLevel2Dto;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static de.rhauswald.security.idor.internal.scanner.DtoFieldInformationWithNameAndValueMatcher.aDtoFieldInformationWithNameAndValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ProtectedFieldScannerTest {
    @Test
    public void testFlatDto() throws Exception {
        final FlatDto dto = new FlatDto();
        dto.setId("23");
        final MutableList<DtoFieldInformation> protectedFields = ProtectedFieldScanner.scanForIdorProtectedFields(dto)
                .toList()
                .sortThis(new DtoFieldInformationComparator());

        assertThat(protectedFields, is(Matchers.<DtoFieldInformation>iterableWithSize(1)));
        final DtoFieldInformation fieldInformation = protectedFields.getFirst();
        assertThat(fieldInformation, is(aDtoFieldInformationWithNameAndValue("id", "23")));
        assertThat(fieldInformation.getDto(), Matchers.<Object>is(dto));
    }

    @Test
    public void testFlatDtoWithNullValueInProtectedField() throws Exception {
        final FlatDto dto = new FlatDto();
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(dto);
        assertThat(dtoFieldInformation, is(Matchers.<DtoFieldInformation>iterableWithSize(0)));
    }

    @Test
    public void testDerivedDto() throws Exception {
        final DerivedDto dto = new DerivedDto();
        dto.setId("23");
        dto.setChildClassId("42");
        final MutableList<DtoFieldInformation> protectedFields = ProtectedFieldScanner.scanForIdorProtectedFields(dto)
                .toList()
                .sortThis(new DtoFieldInformationComparator());

        assertThat(protectedFields, is(Matchers.<DtoFieldInformation>iterableWithSize(2)));
        final MutableList<DtoFieldInformation> sortedFields = protectedFields.toSortedList(new Comparator<DtoFieldInformation>() {
            @Override
            public int compare(DtoFieldInformation o1, DtoFieldInformation o2) {
                final String first = (String) o1.getValue();
                final String second = (String) o2.getValue();
                return first.compareTo(second);
            }
        });
        final DtoFieldInformation dtoFieldInformationFirst = sortedFields.getFirst();
        assertThat(dtoFieldInformationFirst, is(aDtoFieldInformationWithNameAndValue("id", "23")));
        assertThat(dtoFieldInformationFirst.getDto(), Matchers.<Object>is(dto));
        final DtoFieldInformation dtoFieldInformationSecond = sortedFields.getLast();
        assertThat(dtoFieldInformationSecond, is(aDtoFieldInformationWithNameAndValue("childClassId", "42")));
        assertThat(dtoFieldInformationSecond.getDto(), Matchers.<Object>is(dto));
    }

    @Test
    public void testNestedDtos() throws Exception {
        final NestedLevel2Dto nestedLevel2Dto = new NestedLevel2Dto();
        nestedLevel2Dto.setId("99");

        final NestedLevel1Dto nestedLevel1Dto = new NestedLevel1Dto();
        nestedLevel1Dto.setId("42");
        nestedLevel1Dto.setNestedLevel2Dto(nestedLevel2Dto);

        final NestedLevel0Dto nestedLevel0Dto = new NestedLevel0Dto();
        nestedLevel0Dto.setId("23");
        nestedLevel0Dto.setNestedLevel1Dto(nestedLevel1Dto);

        final MutableList<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(nestedLevel0Dto)
                .toList()
                .sortThis(new DtoFieldInformationComparator());
        assertThat(dtoFieldInformation, is(Matchers.<DtoFieldInformation>iterableWithSize(3)));

        final DtoFieldInformation dtoFieldInformationNestedLevel0Dto = dtoFieldInformation.get(0);
        assertThat(dtoFieldInformationNestedLevel0Dto, is(aDtoFieldInformationWithNameAndValue("id", "23")));
        assertThat(dtoFieldInformationNestedLevel0Dto.getDto(), Matchers.<Object>is(nestedLevel0Dto));

        final DtoFieldInformation dtoFieldInformationNestedLevel1Dto = dtoFieldInformation.get(1);
        assertThat(dtoFieldInformationNestedLevel1Dto, is(aDtoFieldInformationWithNameAndValue("id", "42")));
        assertThat(dtoFieldInformationNestedLevel1Dto.getDto(), Matchers.<Object>is(nestedLevel1Dto));

        final DtoFieldInformation dtoFieldInformationNestedLevel2Dto = dtoFieldInformation.get(2);
        assertThat(dtoFieldInformationNestedLevel2Dto, is(aDtoFieldInformationWithNameAndValue("id", "99")));
        assertThat(dtoFieldInformationNestedLevel2Dto.getDto(), Matchers.<Object>is(nestedLevel2Dto));
    }

    @Test
    public void testCyclicDtos() throws Exception {
        final CyclicLevel2Dto nestedLevel2Dto = new CyclicLevel2Dto();
        nestedLevel2Dto.setId("99");

        final CyclicLevel1Dto nestedLevel1Dto = new CyclicLevel1Dto();
        nestedLevel1Dto.setId("42");
        nestedLevel1Dto.setCyclicLevel2Dto(nestedLevel2Dto);

        final CyclicLevel0Dto nestedLevel0Dto = new CyclicLevel0Dto();
        nestedLevel0Dto.setId("23");
        nestedLevel0Dto.setCyclicLevel1Dto(nestedLevel1Dto);
        nestedLevel1Dto.setRoot(nestedLevel0Dto);
        nestedLevel1Dto.setParent(nestedLevel0Dto);
        nestedLevel1Dto.setChild(nestedLevel2Dto);
        nestedLevel2Dto.setRoot(nestedLevel0Dto);
        nestedLevel2Dto.setParent(nestedLevel1Dto);

        final MutableList<DtoFieldInformation> protectedFields = ProtectedFieldScanner.scanForIdorProtectedFields(nestedLevel0Dto)
                .toList()
                .sortThis(new DtoFieldInformationComparator());

        assertThat(protectedFields, is(Matchers.<DtoFieldInformation>iterableWithSize(3)));
        final DtoFieldInformation fieldInformation = protectedFields.getFirst();
        assertThat(fieldInformation, is(aDtoFieldInformationWithNameAndValue("id", "23")));
    }

    @Test
    public void testSetDtos() throws Exception {
        final SetLevel0Dto setLevel0Dto = new SetLevel0Dto();
        setLevel0Dto.setId("23");
        final HashSet<SetLevel1Dto> setLevel1Dtos = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            final SetLevel1Dto setLevel1Dto = new SetLevel1Dto();
            setLevel1Dto.setId(String.valueOf(i));
            setLevel1Dtos.add(setLevel1Dto);
        }
        setLevel0Dto.setSetLevel1Dtos(setLevel1Dtos);
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(setLevel0Dto);
        assertThat(dtoFieldInformation, is(Matchers.<DtoFieldInformation>iterableWithSize(101)));
    }

    @Test
    public void testListDtos() throws Exception {
        final ListLevel0Dto setLevel0Dto = new ListLevel0Dto();
        setLevel0Dto.setId("23");
        final List<ListLevel1Dto> setLevel1Dtos = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            final ListLevel1Dto setLevel1Dto = new ListLevel1Dto();
            setLevel1Dto.setId(String.valueOf(i));
            setLevel1Dtos.add(setLevel1Dto);
        }
        setLevel0Dto.setListLevel1Dtos(setLevel1Dtos);
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(setLevel0Dto);
        assertThat(dtoFieldInformation, is(Matchers.<DtoFieldInformation>iterableWithSize(101)));
    }

    @Test
    public void testMapDtos() throws Exception {
        final MapLevel0Dto setLevel0Dto = new MapLevel0Dto();
        setLevel0Dto.setId("23");
        final HashMap<MapLevel1KeyDto, MapLevel1ValueDto> mapLevel1Dtos = new HashMap<>();
        for (int i = 0; i < 200; i += 2) {
            final MapLevel1KeyDto mapLevel1KeyDto = new MapLevel1KeyDto();
            mapLevel1KeyDto.setId(String.valueOf(i));
            final MapLevel1ValueDto mapLevel1ValueDto = new MapLevel1ValueDto();
            mapLevel1ValueDto.setId(String.valueOf(i + 1));
            mapLevel1Dtos.put(mapLevel1KeyDto, mapLevel1ValueDto);
        }
        setLevel0Dto.setMapLevel1Dtos(mapLevel1Dtos);
        final Stopwatch started = Stopwatch.createStarted();
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(setLevel0Dto);
        System.out.println(started.stop().elapsed(TimeUnit.MILLISECONDS));
        assertThat(dtoFieldInformation, is(Matchers.<DtoFieldInformation>iterableWithSize(201)));
    }

    @Test
    public void testArrayDtos() throws Exception {
        final ArrayLevel0Dto arrayLevel0Dto = new ArrayLevel0Dto();
        arrayLevel0Dto.setId("23");
        final ArrayLevel1Dto[] arrayLevel1Dtos = new ArrayLevel1Dto[100];
        for (int i = 0; i < arrayLevel1Dtos.length; i++) {
            final ArrayLevel1Dto arrayLevel1Dto = new ArrayLevel1Dto();
            arrayLevel1Dto.setId(String.valueOf(i));
            arrayLevel1Dtos[i] = arrayLevel1Dto;
        }
        arrayLevel0Dto.setArrayLevel1Dtos(arrayLevel1Dtos);
        final Stopwatch started = Stopwatch.createStarted();
        final ImmutableSet<DtoFieldInformation> dtoFieldInformation = ProtectedFieldScanner.scanForIdorProtectedFields(arrayLevel0Dto);
        System.out.println(started.elapsed(TimeUnit.MILLISECONDS));
        assertThat(dtoFieldInformation, is(Matchers.<DtoFieldInformation>iterableWithSize(101)));
    }

    private static final class DtoFieldInformationComparator implements Comparator<DtoFieldInformation>, Serializable {
        private static final long serialVersionUID = -3431660366589942807L;

        @Override
        public int compare(DtoFieldInformation o1, DtoFieldInformation o2) {
            final Integer valueO1 = Integer.parseInt((String) o1.getValue());
            final Integer valueO2 = Integer.parseInt((String) o2.getValue());
            return valueO1.compareTo(valueO2);
        }
    }
}
